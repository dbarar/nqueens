package solveNqueens;

import java.util.Scanner;

public class MainNQueens {
    public static void main(String[] args) {
        System.out.println("Input N = ");
        Scanner console = new Scanner(System.in);

        int N = Integer.parseInt(console.next());
        System.out.println("Solving NQueen for " + N + "x" + N + " chessboard");

        String s;

        do {
            System.out.println(" \n Insert: AC1 / AC3 / BT / BTFC / BTLA / BTFCLA / BTAC1 / BTAC3 / CGraphSize and 0 to exit");
            console = new Scanner(System.in);
            s = console.next();

            NQueensSolve nQueensSolve = new NQueensSolve(N);
            nQueensSolve.solve(s);
        }
        while (!s.equals("0"));
    }
}
