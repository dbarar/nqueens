package solveScheduling;

import java.util.Scanner;

public class MainScheduling {

    public static void main(String[] args) {
        String s;

        System.out.println("Solving a problem of scheduling described as follows: ");

        do {
            System.out.println(" \n Insert: AC1 / AC3 / BT / BTFC / BTLA / BTFCLA / BTAC1 / BTAC3 / CGraphSize and 0 to exit");
            Scanner console = new Scanner(System.in);
            s = console.next();

            SchedulingSolve schedulingSolve = new SchedulingSolve();
            schedulingSolve.solve(s);
        }
        while (!s.equals("0"));
    }
}
