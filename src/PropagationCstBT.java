import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.function.Consumer;

public class PropagationCstBT {
    // number of queens
    private int N;
    // spaces
    private int spaces;
    // constraints 0...N2
    private List<Integer> constraints;
    // the final places of the queens
    private List<Integer> queenSpaces;

    public PropagationCstBT(int N) {
        this.N = N;
        this.spaces = N * N;
        this.constraints = new ArrayList<>(this.spaces);
        for (int i = 0; i < spaces; i++) {
            constraints.add(0);
        }
        this.queenSpaces = new ArrayList<>();
    }

    public static void main(String[] args) {
        System.out.println("Input N = ");
        Scanner console = new Scanner(System.in);

        int N = Integer.parseInt(console.next());
        System.out.println("Solving NQueen for " + N + "x" + N + " chessboard");

        PropagationCstBT b = new PropagationCstBT(N);
        int domainSteps = 0;
        int btSteps = 0;
        System.out.println(b.placeMoves(domainSteps, btSteps));
        b.printBoard();
    }

    private List<Integer> getPossibleMoves() {
        List<Integer> possibleMoves = new ArrayList<>();
        for (int i = 0; i < constraints.size(); i++) {
            if (constraints.get(i) == 0)
                possibleMoves.add(i);
        }
        return possibleMoves;
    }

    private void makeMove(int space) {
        // add the queen
        queenSpaces.add(space);

        // add the conflicts
        updateConflicts(space, OpConflicts.ADD);
    }

    private void removeMove(int space) {
        // remove the queen
        queenSpaces.removeIf(i -> i.equals(space));
        // remove the conflicts
        updateConflicts(space, OpConflicts.REM);
    }

    private void updateConflicts(int move, OpConflicts op) {
        Consumer<Integer> opConstraintFct;
        if (op.equals(OpConflicts.ADD)) {
            opConstraintFct = this::addConstraint;
        } else {
            opConstraintFct = this::removeConstraint;
        }

        int row = move / N;
        int col = move % N;
        int rightDiagStartRow = row + col;
        int leftDiagStartRow = row - col;

        for (int i = 0; i < N; i++) {
            // row
            opConstraintFct.accept(rowColumnToSpace(row, i));

            // col
            opConstraintFct.accept(rowColumnToSpace(i, col));

            //  / diag
            if (rightDiagStartRow > -1) {
                opConstraintFct.accept(rowColumnToSpace(rightDiagStartRow, i));
                rightDiagStartRow -= 1;
            }
            // \ diag
            if (leftDiagStartRow < N) {
                opConstraintFct.accept(rowColumnToSpace(leftDiagStartRow, i));
                leftDiagStartRow += 1;
            }
        }
    }

    private void addConstraint(int move) {
        if (move != -1) {
            constraints.set(move, constraints.get(move) + 1);
        }
    }

    private void removeConstraint(int move) {
        if (move != -1) {
            constraints.set(move, constraints.get(move) - 1);
        }
    }

    private int rowColumnToSpace(int row, int col) {
        int space = row * N + col;
        if (space >= spaces || space < 0)
            return -1;
        return space;
    }

    public void printBoard() {
        for (int r = 0; r < N; r++) {
            StringBuilder row = new StringBuilder();
            for (int c = 0; c < N; c++) {
                if (queenSpaces.contains(rowColumnToSpace(r, c))) {
                    row.append("Q");
                } else {
                    row.append("-");
                }
                row.append("  ");
            }
            System.out.println(row);
        }
    }

    public boolean placeMoves(int domainSteps, int btSteps) {
        btSteps++;
        List<Integer> possibleMoves = getPossibleMoves();
        for (Integer move : possibleMoves) {
            domainSteps++;
            makeMove(move);

            if (queenSpaces.size() == N) {
                System.out.println("Domain iterations: " + domainSteps);
                System.out.println("BT iterations: " + btSteps);
                return true;
            } else {
                if (placeMoves(domainSteps, btSteps)) {
                    return true;
                } else {
                    removeMove(move);
                }
            }
        }
        return false;
    }


    public enum OpConflicts {
        ADD,
        REM
    }
}
