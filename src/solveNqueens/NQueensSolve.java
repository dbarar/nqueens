package solveNqueens;
import javafx.util.Pair;

import java.util.*;
import java.util.concurrent.ConcurrentSkipListSet;

public class NQueensSolve {

    private int N;
    private List<Integer> variables = new ArrayList<>(); //contains the rows // assignments
    private List<Queen> queenList = new ArrayList<>();
    private List<Pair<Integer, Integer>> constraints = new ArrayList<>();
    private List<Pair<Integer, Integer>> solution; //contains the rows // assignments

    public NQueensSolve(int N) {
        this.N = N;
        initVariables();
        solution = initSolution();
        initQueens();
        computeConstraints();
    }

    public void solve(String cmd) {
        switch (cmd) {
            case "CGraphSize":
                System.out.println("Constraint graph ");
                System.out.println("No of nodes: " + N);
                System.out.println("No of arcs: " + constraints.size());
                System.out.println("No of edges: " + constraints.size() / 2);
                System.out.println("All Domain size: " + N * N);
                break;
            case "AC1":
                if (ac1()) {
                    System.out.println("AC1 optimized the domain:");
                    printQueens();
                } else {
                    System.out.println("AC1 couldn't find any domain optimizations");
                }
                break;
            case "AC3":
                if (ac3()) {
                    System.out.println("AC3 optimized the domain:");
                    printQueens();
                } else {
                    System.out.println("AC3 couldn't find any domain optimizations");
                }
                break;
            case "BT":
                int domainIterations = 0;
                int btIterations = 0;
                backtracking(domainIterations, btIterations);
                printQueensPretty();
                break;
            case "BTFC":
                domainIterations = 0;
                btIterations = 0;
                backtrackingFC(domainIterations, btIterations);
                printQueensPretty();
                break;
            case "BTLA":
                domainIterations = 0;
                btIterations = 0;
                backtrackingLA(domainIterations, btIterations);
                printQueensPretty();
                break;
            case "BTFCLA":
                domainIterations = 0;
                btIterations = 0;
                backtrackingFCLA(domainIterations, btIterations);
                printQueensPretty();
                break;
            case "BTAC1":
                domainIterations = 0;
                btIterations = 0;
                ac1();
                backtracking(domainIterations, btIterations);
                printQueensPretty();
                break;
            case "BTAC3":
                domainIterations = 0;
                btIterations = 0;
                ac3();
                backtracking(domainIterations, btIterations);
                printQueensPretty();
                break;
            default:
                System.out.println("Insert a proper command! ");
        }
    }

    private void initVariables() {
        for (int i = 0; i < N; i++) {
            variables.add(i);
        }
//        System.out.println(variables);
    }

    private List<Pair<Integer, Integer>> initSolution() {
        List<Pair<Integer, Integer>> solution = new ArrayList<>();
        for (int i = 0; i < N; i++) {
            solution.add(new Pair<>(i, -1));
        }
        return solution;
    }

    private void initQueens() {
        for (int col = 0; col < this.N; col++) {
            Queen queen = new Queen(this.N, col, variables);
            queenList.add(queen);
        }
        printQueens();
    }

    public void computeConstraints() {
        for (Queen q : queenList) {
            for (Integer n : q.getNeighbours()) {
                constraints.add(new Pair<>(q.getCol(), n));
            }
        }
        System.out.println(constraints);
    }

    public void printQueens() {
        for (Queen q : queenList) {
            System.out.println(q);
        }
    }

    public void printQueensPretty() {
        if (!isSolutionComplete()) {
            System.out.println("A solution was not found");
            return;
        }
        int[][] solutionArray = new int[N][N];
        for (Queen q : queenList) {
            solutionArray[q.getRow()][q.getCol()] = q.getCol() + 1;
        }
        for (int[] r : solutionArray) {
            System.out.println(Arrays.toString(r));
        }
    }

    private List<Pair<Integer, Integer>> backtracking(int domainIterations, int btIterations) {
        btIterations++;
        if (isSolutionComplete()) {
            System.out.println("Domain iterations: " + domainIterations);
            System.out.println("BT iterations: " + domainIterations);
            return solution;
        }

        int queenIdx = 0;
        for (int x = 0; x < N; x++) {
            if (solution.get(x).getValue() == -1) {
                queenIdx = x;
                break;
            }
        }

        Queen q = queenList.get(queenIdx);
        for (int val : q.getDomain()) {
            domainIterations++;
            if (isValid(solution, queenIdx, val)) {

                // do move
                solution.set(queenIdx, new Pair<>(queenIdx, val));
                updateMoveQueen(q, val);

                if (backtracking(domainIterations, btIterations) != null) {
                    return solution;
                }
                // undo move
                solution.set(queenIdx, new Pair<>(queenIdx, -1));
                updateUndoQueen(q);
            }
        }
        return null;
    }

    private List<Pair<Integer, Integer>> backtrackingFC(int domainIterations, int btIterations) {
        btIterations++;
        if (isSolutionComplete()) {
            System.out.println("Domain iterations: " + domainIterations);
            System.out.println("BT iterations: " + btIterations);
            return solution;
        }

        int queenIdx = 0;
        for (int x = 0; x < N; x++) {
            if (solution.get(x).getValue() == -1) {
                queenIdx = x;
                break;
            }
        }

        Queen q = queenList.get(queenIdx);
        for (int val : q.getDomain()) {
            domainIterations++;
            if (isValid(solution, queenIdx, val)) {
                // forward check
                if (forwardCheck(q, val)) {
                    for (Queen qq : queenList) {
                        if (qq != q && qq.getCol() > q.getCol()) {
                            updateUndoQueen(qq);
                        }
                    }
                } else {
                    // do move
                    solution.set(queenIdx, new Pair<>(queenIdx, val));
                    updateMoveQueen(q, val);

                    if (backtrackingFC(domainIterations, btIterations) != null) {
                        return solution;
                    }
                    // undo move
                    solution.set(queenIdx, new Pair<>(queenIdx, -1));
                    updateUndoQueen(q);
                }
            }
        }
        return null;
    }

    private List<Pair<Integer, Integer>> backtrackingLA(int domainIterations, int btIterations) {
        btIterations++;
        if (isSolutionComplete()) {
            System.out.println("Domain iterations: " + domainIterations);
            System.out.println("BT iterations: " + btIterations);
            return solution;
        }

        int queenIdx = 0;
        for (int x = 0; x < N; x++) {
            if (solution.get(x).getValue() == -1) {
                queenIdx = x;
                break;
            }
        }

        Queen q = queenList.get(queenIdx);
        for (int val : q.getDomain()) {
            domainIterations++;
            if (isValid(solution, queenIdx, val)) {
                // do move
                solution.set(queenIdx, new Pair<>(queenIdx, val));
                updateMoveQueen(q, val);

                // look ahead
                if (lookAhead()) {
                    for (Queen qq : queenList) {
                        if (qq.getCol() > q.getCol()) {
                            updateUndoQueen(qq);
                        }
                    }
                    solution.set(queenIdx, new Pair<>(queenIdx, -1));
                    q.setRow(null);
                } else {
                    if (backtrackingLA(domainIterations, btIterations) != null) {
                        return solution;
                    }

                    // undo move
                    updateUndoQueen(q);
                    solution.set(queenIdx, new Pair<>(queenIdx, -1));
                }
            }
        }
        return null;
    }

    private List<Pair<Integer, Integer>> backtrackingFCLA(int domainIterations, int btIterations) {
        btIterations++;
        if (isSolutionComplete()) {
            System.out.println("Domain iterations: " + domainIterations);
            System.out.println("BT iterations: " + btIterations);
            return solution;
        }

        int queenIdx = 0;
        for (int x = 0; x < N; x++) {
            if (solution.get(x).getValue() == -1) {
                queenIdx = x;
                break;
            }
        }

        Queen q = queenList.get(queenIdx);
        for (int val : q.getDomain()) {
            domainIterations++;
            if (isValid(solution, queenIdx, val)) {
                // forward check
                if (forwardCheck(q, val)) {
                    for (Queen qq : queenList) {
                        if (qq != q && qq.getCol() > q.getCol()) {
                            updateUndoQueen(qq);
                        }
                    }
                } else {
                    // do move
                    solution.set(queenIdx, new Pair<>(queenIdx, val));
                    updateMoveQueen(q, val);

                    // look ahead
                    if (lookAhead()) {
                        for (Queen qq : queenList) {
                            if (qq.getCol() > q.getCol()) {
                                updateUndoQueen(qq);
                            }
                        }
                        solution.set(queenIdx, new Pair<>(queenIdx, -1));
                        q.setRow(null);
                    } else {
                        if (backtrackingFCLA(domainIterations, btIterations) != null) {
                            return solution;
                        }

                        // undo move
                        updateUndoQueen(q);
                        solution.set(queenIdx, new Pair<>(queenIdx, -1));
                    }
                }
            }
        }
        return null;
    }

    private boolean forwardCheck(Queen q, int q1Raw) {
        int q1Col = q.getCol();
        for (Queen futureQ : queenList) {
            if (q != futureQ && solution.get(futureQ.getCol()).getValue() == -1) {
                int q2Col = futureQ.getCol();
                for (int q2Raw : futureQ.getDomain()) {
                    if ((q1Col != q2Col) && (q1Raw == q2Raw || q1Col + q1Raw == q2Col + q2Raw || q1Col - q1Raw == q2Col - q2Raw))
                        futureQ.getDomain().remove(q2Raw);
                }
                if (futureQ.getDomain().isEmpty())
                    return true;
            }
        }
        return false;
    }

    private boolean lookAhead() {
        for (int q1Col = 0; q1Col < N; q1Col++) {
            if (solution.get(q1Col).getValue() != -1) {
                continue;
            }
            Queen q1 = queenList.get(q1Col);
            for (int q2Col = q1Col + 1; q2Col < N; q2Col++) {
                if (solution.get(q1Col).getValue() != -1) {
                    continue;
                }
                Queen q2 = queenList.get(q2Col);

                // test compatibility
                for (int q1Raw : q1.getDomain()) {
                    boolean consistent = false;
                    for (int q2Raw : q2.getDomain()) {
                        if (!((q1Col != q2Col) && (q1Raw == q2Raw || q1Col + q1Raw == q2Col + q2Raw || q1Col - q1Raw == q2Col - q2Raw))) {
                            consistent = true;
                            break;
                        }
                    }
                    if (!consistent) {
                        q2.getDomain().removeIf(i -> i.equals(q1Raw));
                    }
                    if (q1.getDomain().isEmpty() || q2.getDomain().isEmpty()) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private void updateMoveQueen(Queen q, int val) {
        q.getDomain().removeIf(i -> i == val);
        q.setRow(val);
    }

    private void updateUndoQueen(Queen q) {
        ConcurrentSkipListSet<Integer> domain = new ConcurrentSkipListSet<>(variables);
        q.setDomain(domain);
        q.setRow(null);
    }

    private boolean isSolutionComplete() {
        for (int i = 0; i < N; i++) {
            if (solution.get(i).getValue() == -1)
                return false;
        }
        return true;
    }

    private boolean isValid(List<Pair<Integer, Integer>> solution, int queenIdx, int val) {
        List<Pair<Integer, Integer>> solutionCopy = new ArrayList<>(solution);

        solutionCopy.set(queenIdx, new Pair<>(queenIdx, val));

        for (Pair<Integer, Integer> pair1 : solutionCopy) {
            for (Pair<Integer, Integer> pair2 : solutionCopy) {
                int q1Raw = pair1.getValue();
                int q2Raw = pair2.getValue();
                int q1Col = pair1.getKey();
                int q2Col = pair2.getKey();

                if (q1Raw == -1 || q2Raw == -1)
                    continue;

                if ((q1Col != q2Col) && (q1Raw == q2Raw || q1Col + q1Raw == q2Col + q2Raw || q1Col - q1Raw == q2Col - q2Raw))
                    return false;
            }
        }
        return true;
    }

    private boolean ac1() {
        List<Pair<Integer, Integer>> qConstraints = new ArrayList<>(constraints);
        int ac1Steps = 0;
        boolean changed;
        int limitTries = N;
        do {
            changed = false;
            for (Pair<Integer, Integer> pair : qConstraints) {
                ac1Steps++;
                changed = revise(pair.getKey(), pair.getValue()) || changed;
            }
            limitTries--;

        } while (!changed && limitTries >= 0);
        System.out.println("AC1 Steps: " + ac1Steps);
        return changed;
    }

    private boolean ac3() {
        Queue<Pair<Integer, Integer>> queue = new ArrayDeque<>(constraints);
        int ac3Steps = 0;
        boolean changed = false;
        while (queue.size() > 0) {
            ac3Steps++;
            Pair<Integer, Integer> pair = queue.poll();
            int x1 = pair.getKey();
            int xj = pair.getValue();

            if (revise(x1, xj)) {
                changed = true;
                Queen q = queenList.get(x1);
                if (q.getDomain().size() == 0) {
                    System.out.println("No solution could be found, a domain of a queen was deleted");
                    break;
                }
                for (int xk : q.getNeighbours()) {
                    if (xk == xj) {
                        continue;
                    }
                    queue.add(new Pair<>(xk, x1));
                }
            }
        }
        System.out.println("AC3 Steps: " + ac3Steps);
        return changed;
    }

    private boolean revise(int x1, int x2) {
        boolean deleted = false;
        Queen q1 = queenList.get(x1);
        Queen q2 = queenList.get(x2);
        int q1Col = q1.getCol();
        int q2Col = q2.getCol();
        ConcurrentSkipListSet<Integer> presentDomain1 = new ConcurrentSkipListSet<>();
        if (q1.getRow() != null) {
            presentDomain1.add(q1.getRow());
        } else {
            presentDomain1 = q1.getDomain();
        }

        ConcurrentSkipListSet<Integer> presentDomain2 = new ConcurrentSkipListSet<>();
        if (q2.getRow() != null) {
            presentDomain2.add(q2.getRow());
        } else {
            presentDomain2 = q2.getDomain();
        }

        for (int q1Raw : presentDomain1) {
            boolean existConsistencies = false;
            for (int q2Raw : presentDomain2) {
                if (((q1Col != q2Col) && (q1Raw == q2Raw || q1Col + q1Raw == q2Col + q2Raw || q1Col - q1Raw == q2Col - q2Raw))) {
                    existConsistencies = true;
                    break;
                }
            }
            if (!existConsistencies) {
                q1.getDomain().removeIf(i -> i == q1Raw);
                deleted = true;
            }
        }
        return deleted;
    }
}