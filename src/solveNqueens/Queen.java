package solveNqueens;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentSkipListSet;

public class Queen {

    private ConcurrentSkipListSet<Integer> domain;

    private Integer row;
    private Integer col;

    private List<Integer> neighbours = new ArrayList<>();

    public Queen(int N, int col, List<Integer> variables) {
        domain = new ConcurrentSkipListSet<>(variables);
        this.col = col;
        initNeighbours(N, col);
    }

    private void initNeighbours(int N, int col) {
        for (int i = 0; i < N; i++) {
            if (i != col) {
                neighbours.add(i);
            }
        }
    }

    public ConcurrentSkipListSet<Integer> getDomain() {
        return domain;
    }

    public void setDomain(ConcurrentSkipListSet<Integer> domain) {
        this.domain = domain;
    }

    public Integer getRow() {
        return row;
    }

    public void setRow(Integer row) {
        this.row = row;
    }

    public Integer getCol() {
        return col;
    }

    public List<Integer> getNeighbours() {
        return neighbours;
    }

    @Override
    public String toString() {
        return "Queen{" +
                "domain=" + domain +
                ", row=" + row +
                ", col=" + col +
                ", neighbours=" + neighbours +
                '}';
    }
}
