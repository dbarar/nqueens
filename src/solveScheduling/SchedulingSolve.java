package solveScheduling;

import javafx.util.Pair;

import java.util.*;
import java.util.concurrent.ConcurrentSkipListSet;

public class SchedulingSolve {

    int N;
    private List<Integer> variables;
    private List<Job> jobList;
    private Set<Pair<Integer, Integer>> constraints = new HashSet<>();
    private List<Pair<Integer, Integer>> solution = new ArrayList<>();

    public SchedulingSolve() {
        initialize();
    }

    private void initialize() {
        N = 7;
        variables = Arrays.asList(1, 2, 3, 4);
        Job job0 = new Job(0, new ConcurrentSkipListSet<>(variables), Arrays.asList(1, 6));         //B
        Job job1 = new Job(1, new ConcurrentSkipListSet<>(variables), Arrays.asList(0, 2, 3, 4, 6));//N
        Job job2 = new Job(2, new ConcurrentSkipListSet<>(Collections.singletonList(1)), Arrays.asList(1, 3));  //T
        Job job3 = new Job(3, new ConcurrentSkipListSet<>(variables), Arrays.asList(1, 2, 4, 6));   //L
        Job job4 = new Job(4, new ConcurrentSkipListSet<>(variables), Arrays.asList(1, 3, 5, 6));   //P
        Job job5 = new Job(5, new ConcurrentSkipListSet<>(variables), Collections.singletonList(4));            //S
        Job job6 = new Job(6, new ConcurrentSkipListSet<>(variables), Arrays.asList(0, 1, 3, 4));   //C

        jobList = Arrays.asList(job0, job1, job2, job3, job4, job5, job6);

        initConstraints();

        for (int i = 0; i < N; i++) {
            solution.add(new Pair<>(i, -1));
        }

        printJobs();
    }

    private void initConstraints() {
        for (Job j : jobList) {
            for (int i : j.getConflictJobs()) {
                constraints.add(new Pair<>(j.getId(), i));
            }
        }
    }

    public void solve(String cmd) {
        switch (cmd) {
            case "CGraphSize":
                System.out.println("Constraint graph ");
                System.out.println("No of nodes: " + N);
                System.out.println("No of arcs: " + constraints.size());
                System.out.println("No of edges: " + constraints.size() / 2);
                System.out.println("All Domain size: " + variables.size() * 6 + 1);
                break;
            case "AC1":
                if (ac1()) {
                    System.out.println("AC1 optimized the domain: ");
                    printJobs();
                } else {
                    System.out.println("AC1 couldn't find any domain optimizations");
                }
                break;
            case "AC3":
                if (ac3()) {
                    System.out.println("AC3 optimized the domain: ");
                    printJobs();
                } else {
                    System.out.println("AC3 couldn't find any domain optimizations");
                }
                break;
            case "BT":
                int domainIterations = 0;
                int btIterations = 0;
                backtracking(domainIterations, btIterations);
                printJobsPretty();
                break;
            case "BTFC":
                domainIterations = 0;
                btIterations = 0;
                backtrackingFC(domainIterations, btIterations);
                printJobsPretty();
                break;
            case "BTLA":
                domainIterations = 0;
                btIterations = 0;
                backtrackingLA(domainIterations, btIterations);
                printJobsPretty();
                break;
            case "BTFCLA":
                domainIterations = 0;
                btIterations = 0;
                backtrackingFCLA(domainIterations, btIterations);
                printJobsPretty();
                break;
            case "BTAC1":
                domainIterations = 0;
                btIterations = 0;
                ac1();
                backtracking(domainIterations, btIterations);
                printJobsPretty();
                break;
            case "BTAC3":
                domainIterations = 0;
                btIterations = 0;
                ac3();
                backtracking(domainIterations, btIterations);
                printJobsPretty();
                break;
            default:
                System.out.println("Insert a proper command! ");
        }
    }

    private List<Pair<Integer, Integer>> backtracking(int domainIterations, int btIterations) {
        btIterations++;
        if (isSolutionComplete()) {
            System.out.println("Domain iterations: " + domainIterations);
            System.out.println("BT iterations: " + btIterations);
            return solution;
        }

        int jobIdx = 0;
        for (int x = 0; x < N; x++) {
            if (solution.get(x).getValue() == -1) {
                jobIdx = x;
                break;
            }
        }

        Job q = jobList.get(jobIdx);
        for (int val : q.getDomain()) {
            domainIterations++;
            if (isValid(solution, jobIdx, val)) {
                // do move
                solution.set(jobIdx, new Pair<>(jobIdx, val));
                updateMoveJob(q, val);

                if (backtracking(domainIterations, btIterations) != null) {
                    return solution;
                }

                // undo move
                solution.set(jobIdx, new Pair<>(jobIdx, -1));
                updateUndoJob(q);
            }
        }
        return null;
    }

    private List<Pair<Integer, Integer>> backtrackingFC(int domainIterations, int btIterations) {
        btIterations++;
        if (isSolutionComplete()) {
            System.out.println("Domain iterations: " + domainIterations);
            System.out.println("BT iterations: " + btIterations);
            return solution;
        }

        int jobIdx = 0;
        for (int x = 0; x < N; x++) {
            if (solution.get(x).getValue() == -1) {
                jobIdx = x;
                break;
            }
        }

        Job q = jobList.get(jobIdx);
        for (int val : q.getDomain()) {
            domainIterations++;
            if (isValid(solution, jobIdx, val)) {
                // forward check
                if (forwardCheck(q, val)) {
                    for (Job qq : jobList) {
                        if (qq != q && qq.getId() > q.getId()) {
                            updateUndoJob(qq);
                        }
                    }
                } else {
                    // do move
                    solution.set(jobIdx, new Pair<>(jobIdx, val));
                    updateMoveJob(q, val);

                    if (backtrackingFC(domainIterations, btIterations) != null) {
                        return solution;
                    }

                    // undo move
                    solution.set(jobIdx, new Pair<>(jobIdx, -1));
                    updateUndoJob(q);
                }
            }
        }
        return null;
    }

    private List<Pair<Integer, Integer>> backtrackingLA(int domainIterations, int btIterations) {
        btIterations++;
        if (isSolutionComplete()) {
            System.out.println("Domain iterations: " + domainIterations);
            System.out.println("BT iterations: " + btIterations);
            return solution;
        }

        int jobIdx = 0;
        for (int x = 0; x < N; x++) {
            if (solution.get(x).getValue() == -1) {
                jobIdx = x;
                break;
            }
        }

        Job q = jobList.get(jobIdx);
        for (int val : q.getDomain()) {
            domainIterations++;
            if (isValid(solution, jobIdx, val)) {
                // do move
                solution.set(jobIdx, new Pair<>(jobIdx, val));
                updateMoveJob(q, val);

                // look ahead
                if (lookAhead()) {
                    for (Job qq : jobList) {
                        if (qq.getId() > q.getId()) {
                            updateUndoJob(qq);
                        }
                    }
                    solution.set(jobIdx, new Pair<>(jobIdx, -1));
                    q.setVal(null);
                } else {
                    if (backtrackingLA(domainIterations, btIterations) != null) {
                        return solution;
                    }

                    // undo move
                    solution.set(jobIdx, new Pair<>(jobIdx, -1));
                    updateUndoJob(q);
                }
            }
        }
        return null;
    }

    private List<Pair<Integer, Integer>> backtrackingFCLA(int domainIterations, int btIterations) {
        btIterations++;
        if (isSolutionComplete()) {
            System.out.println("Domain iterations: " + domainIterations);
            System.out.println("BT iterations: " + btIterations);
            return solution;
        }

        int jobIdx = 0;
        for (int x = 0; x < N; x++) {
            if (solution.get(x).getValue() == -1) {
                jobIdx = x;
                break;
            }
        }

        Job q = jobList.get(jobIdx);
        for (int val : q.getDomain()) {
            domainIterations++;
            if (isValid(solution, jobIdx, val)) {
                // forward check
                if (forwardCheck(q, val)) {
                    for (Job qq : jobList) {
                        if (qq != q && qq.getId() > q.getId()) {
                            updateUndoJob(qq);
                        }
                    }
                } else {
                    // do move
                    solution.set(jobIdx, new Pair<>(jobIdx, val));
                    updateMoveJob(q, val);

                    // look ahead
                    if (lookAhead()) {
                        for (Job qq : jobList) {
                            if (qq.getId() > q.getId()) {
                                updateUndoJob(qq);
                            }
                        }
                        solution.set(jobIdx, new Pair<>(jobIdx, -1));
                        q.setVal(null);
                    } else {
                        if (backtrackingFCLA(domainIterations, btIterations) != null) {
                            return solution;
                        }

                        // undo move
                        solution.set(jobIdx, new Pair<>(jobIdx, -1));
                        updateUndoJob(q);
                    }
                }
            }
        }
        return null;

    }

    private boolean ac1() {
        List<Pair<Integer, Integer>> jobConstraints = new ArrayList<>(constraints);
        int ac1Steps = 0;
        boolean changed;
        do {
            changed = false;
            for (Pair<Integer, Integer> pair : jobConstraints) {
                ac1Steps++;
                changed = revise(pair.getKey(), pair.getValue()) || changed;
                if (jobList.get(pair.getKey()).getDomain().isEmpty() || jobList.get(pair.getKey()).getDomain().isEmpty()) {
                    return false;
                }
            }
        } while (!changed);
        System.out.println("AC1 Steps: " + ac1Steps);
        return true;
    }

    private boolean ac3() {
        Queue<Pair<Integer, Integer>> queue = new ArrayDeque<>(constraints);
        int ac3Steps = 0;
        boolean changed = false;
        while (queue.size() > 0) {
            ac3Steps++;
            Pair<Integer, Integer> pair = queue.poll();
            int j1 = pair.getKey();
            int j2 = pair.getValue();

            if (revise(j1, j2)) {
                changed = true;
                Job q = jobList.get(j1);
                if (q.getDomain().size() == 0) {
                    System.out.println("No solution could be found, a domain of a queen was deleted");
                    break;
                }
                for (int xk : q.getConflictJobs()) {
                    if (xk == j2) {
                        continue;
                    }
                    queue.add(new Pair<>(xk, j1));
                }
            }
        }
        System.out.println("AC3 Steps: " + ac3Steps);
        return changed;
    }

    private boolean revise(Integer job1Idx, Integer job2Idx) {
        boolean deleted = false;
        Job j1 = jobList.get(job1Idx);
        Job j2 = jobList.get(job2Idx);

        if (j1.getDomain().size() == 1) {
            deleted = true;
            j2.removeFromDomain(j1.getDomain().first());
        }

        return deleted;
    }

    private boolean forwardCheck(Job curJob, int val) {
        for (Job job : jobList) {
            if (job != curJob && solution.get(job.getId()).getValue() == -1
                    && constraints.contains(new Pair<>(curJob.getId(), job.getId())) && job.getDomain().contains(val)) {
                job.removeFromDomain(val);
            }
            if (job.getDomain().isEmpty() && job.getVal() == null) {
                return true;
            }
        }
        return false;
    }

    private boolean lookAhead() {
        for (Job job1 : jobList) {
            if (solution.get(job1.getId()).getValue() != -1) {
                continue;
            }
            for (int job2Idx : job1.getConflictJobs()) {
                if (solution.get(job2Idx).getValue() != -1) {
                    continue;
                }
                Job job2 = jobList.get(job2Idx);

                // test compatibility
                if (job1.getDomain().size() == 1) {
                    job2.removeFromDomain(job1.getDomain().first());
                }
                if (job2.getDomain().isEmpty()) {
                    return true;
                }
            }
        }
        return false;
    }

    private boolean isSolutionComplete() {
        for (int i = 0; i < N; i++) {
            if (solution.get(i).getValue() == -1)
                return false;
        }
        return true;
    }

    private boolean isValid(List<Pair<Integer, Integer>> solution, int jobIdx, int val) {
        List<Pair<Integer, Integer>> solutionCopy = new ArrayList<>(solution);

        solutionCopy.set(jobIdx, new Pair<>(jobIdx, val));

        for (Pair<Integer, Integer> pair1 : solutionCopy) {
            for (Pair<Integer, Integer> pair2 : solutionCopy) {
                int job1val = pair1.getValue();
                int job2val = pair2.getValue();
                int jobId1 = pair1.getKey();
                int jobId2 = pair2.getKey();

                if (job1val == -1 || job2val == -1)
                    continue;

                if (jobId1 != jobId2 && job1val == job2val && constraints.contains(new Pair<>(jobId1, jobId2))) {
                    return false;
                }
            }
        }
        return true;
    }

    private void updateMoveJob(Job q, int val) {
        q.getDomain().removeIf(i -> i == val);
        q.setVal(val);
    }

    private void updateUndoJob(Job q) {
        ConcurrentSkipListSet<Integer> domain;
        if (q.getId() == 2) {
            domain = new ConcurrentSkipListSet<>(Collections.singletonList(1));
        } else {
            domain = new ConcurrentSkipListSet<>(variables);
        }
        q.setDomain(domain);
        q.setVal(null);
    }

    public void printJobsPretty() {
        System.out.println(solution);
        if (!isSolutionComplete()) {
            System.out.println("A solution was not found");
            return;
        }
        int[][] solutionArray = new int[variables.size()][N];
        for (Job q : jobList) {
            solutionArray[q.getVal() - 1][q.getId()] = q.getId() + 1;
        }
        for (int[] r : solutionArray) {
            System.out.println(Arrays.toString(r));
        }
    }

    private void printJobs() {
        for (Job q : jobList) {
            System.out.println(q);
        }
    }
}
