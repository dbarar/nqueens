package solveScheduling;

import java.util.List;
import java.util.concurrent.ConcurrentSkipListSet;

public class Job {

    // 0, 1, .., 6
    private Integer id;
    private ConcurrentSkipListSet<Integer> domain;
    // 1, ..., 4 from domain
    private Integer val;
    private List<Integer> conflictJobs;

    public Job(Integer id, ConcurrentSkipListSet<Integer> domain, List<Integer> conflictJobs) {
        this.domain = domain;
        this.id = id;
        this.conflictJobs = conflictJobs;
    }

    public Integer getId() {
        return id;
    }

    public void removeFromDomain(int val) {
        domain.removeIf(it -> it.equals(val));
    }

    public ConcurrentSkipListSet<Integer> getDomain() {
        return domain;
    }

    public void setDomain(ConcurrentSkipListSet<Integer> domain) {
        this.domain = domain;
    }

    public Integer getVal() {
        return val;
    }

    public void setVal(Integer val) {
        this.val = val;
    }

    public List<Integer> getConflictJobs() {
        return conflictJobs;
    }

    @Override
    public String toString() {
        return "Job{" +
                "id=" + id +
                ", domain=" + domain +
                ", val=" + val +
                ", conflictJobs=" + conflictJobs +
                '}';
    }
}
